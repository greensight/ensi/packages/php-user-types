<?php

namespace Ensi\UserTypes;

class UserType
{
    public const ADMIN = 'admin';
    public const CUSTOMER = 'customer';
    public const SELLER = 'seller';

    public static function cases(): array
    {
        return [self::ADMIN, self::CUSTOMER, self::SELLER];
    }
}
