# User Types

Enum with user types for connecting to any Ensi service

## Installation

You can install the package via composer:

`composer require ensi/user-types`

## Basic usage

```php
use Ensi\UserTypes\UserType;

echo UserType::ADMIN; // admin
echo UserType::CUSTOMER; // customer
echo UserType::SELLER; // seller

var_dump(UserType::cases()); // ['admin', 'customer', 'seller']
```

### Testing

1. composer install
2. npm i
3. composer test

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
