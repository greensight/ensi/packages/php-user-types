<?php

use Ensi\UserTypes\UserType;

test('constants are available', function () {
    expect(UserType::ADMIN)->toEqual('admin');
    expect(UserType::CUSTOMER)->toEqual('customer');
    expect(UserType::SELLER)->toEqual('seller');
});

test('cases method returns all', function () {
    expect(UserType::cases())->toEqual(['admin', 'customer', 'seller']);
});
